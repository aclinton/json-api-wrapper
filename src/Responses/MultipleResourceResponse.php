<?php

namespace PandaMan\JsonApiWrapper\Responses;

use PandaMan\JsonApiWrapper\Resources\JsonResource;
use PandaMan\JsonApiWrapper\Traits\ResponseDataTrait;
use Psr\Http\Message\ResponseInterface;

/**
 * Class MultipleResourceResponse
 *
 * @package PandaMan\JsonApiWrapper\Responses
 */
abstract class MultipleResourceResponse
{
    use ResponseDataTrait;

    protected array $data = [];

    /**
     * Get Data
     *
     * Define what the root data point is. This will be passed into the base resource constructor
     */
    abstract protected function getData(array $base_data): array;

    /**
     * Get Base Resource
     *
     * Define what the base resource object is.
     */
    abstract protected function getBaseResource(): string;

    public function __construct(ResponseInterface $response)
    {
        $this->setUp($response);
        $this->prepareResource();
    }

    protected function setUp(ResponseInterface $response): void
    {
        $this->setUpData($response);
    }

    public function data(): array
    {
        return $this->data;
    }

    protected function prepareResource()
    {
        $class = $this->getBaseResource();

        foreach ($this->getData(json_decode($this->rawBody(), true)) as $item) {
            // note we are constructing in override mode so the class has true as a second property
            $this->data[] = new $class($item, true);
        }
    }
}