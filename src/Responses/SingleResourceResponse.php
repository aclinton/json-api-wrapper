<?php

namespace PandaMan\JsonApiWrapper\Responses;

use PandaMan\JsonApiWrapper\Resources\JsonResource;
use PandaMan\JsonApiWrapper\Traits\ResponseDataTrait;
use Psr\Http\Message\ResponseInterface;

/**
 * Class SingleResourceResponse
 *
 * @package PandaMan\JsonApiWrapper\Responses
 */
abstract class SingleResourceResponse
{
    use ResponseDataTrait;

    protected ?JsonResource $data = null;

    public function __construct(ResponseInterface $response)
    {
        $this->setUp($response);
        $this->prepareResource();
    }

    public function data(): ?JsonResource
    {
        return $this->data;
    }

    protected function setUp(ResponseInterface $response): void
    {
        $this->setUpData($response);

        if ($this->body()->errors ?? false) {
            return;
        }
    }

    protected function prepareResource()
    {
        $class = $this->getBaseResource();
        $data  = json_decode($this->rawBody(), true);

        if (sizeof($data)) {
            $data = $this->getData($data);
            // note we are constructing in override mode so the class has true as a second property
            $this->data = new $class($data, true);
        }
    }

    /**
     * Get Data
     *
     * Define what the root data point is. This will be passed into the base resource constructor
     */
    abstract protected function getData(array $base_data): array;

    /**
     * Get Base Resource
     *
     * Define what the base resource object is.
     */
    abstract protected function getBaseResource(): string;
}