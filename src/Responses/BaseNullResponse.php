<?php


namespace PandaMan\JsonApiWrapper\Responses;


use PandaMan\JsonApiWrapper\Traits\ResponseDataTrait;
use Psr\Http\Message\ResponseInterface;

abstract class BaseNullResponse
{
    use ResponseDataTrait;

    public function __construct(ResponseInterface $response)
    {
        $this->setUpData($response);
    }
}