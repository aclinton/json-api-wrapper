<?php


namespace PandaMan\JsonApiWrapper\Responses;


use PandaMan\JsonApiWrapper\Traits\ResponseDataTrait;
use Psr\Http\Message\ResponseInterface;

abstract class BaseCountResponse
{
    use ResponseDataTrait;

    public function __construct(ResponseInterface $response)
    {
        $this->setUpData($response);
    }

    abstract public function count(): ?int;
}