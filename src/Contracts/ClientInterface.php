<?php

namespace PandaMan\JsonApiWrapper\Contracts;


use Psr\Http\Message\ResponseInterface;

/**
 * Interface ClientInterface
 *
 * Contract for implementing full CRUD client object
 *
 * @package PandaMan\JsonApiWrapper\Contracts
 */
interface ClientInterface
{
    /**
     * Get
     *
     * Perform GET request and return response
     *
     * @param string $uri
     * @param array  $query_params
     * @return ResponseInterface
     */
    public function get(string $uri, array $query_params = []): ResponseInterface;

    /**
     * Post
     *
     * Perform POST request and return response
     *
     * @param string $uri
     * @param array  $json_payload
     * @return ResponseInterface
     */
    public function post(string $uri, array $json_payload): ResponseInterface;

    /**
     * Post
     *
     * Perform PUT request and return response
     *
     * @param string $uri
     * @param array  $json_payload
     * @return ResponseInterface
     */
    public function put(string $uri, array $json_payload): ResponseInterface;

    /**
     * Delete
     *
     * Perform DELETE request and return response
     *
     * @param string $uri
     * @return ResponseInterface
     */
    public function delete(string $uri): ResponseInterface;
}