<?php


namespace PandaMan\JsonApiWrapper\Contracts;

interface PropertyUtilityInterface
{
    /**
     * Get Value
     *
     * @param array $property
     * @return mixed
     * @throws \Exception
     */
    public static function getValue(array $property);

    /**
     * Set Value
     *
     * Set value for property
     *
     * @param array $property
     * @param       $value
     * @param bool  $override
     * @return array
     * @throws \Exception
     */
    public static function setValue(array $property, $value, $override = false): array;

    /**
     * Get Value If Changed
     *
     * @param       $property_name
     * @param array $property
     * @return mixed
     */
    public static function getValueIfChanged(string $property_name, array $property): array;

    /**
     * Get Value If Set
     *
     * @param string $property_name
     * @param array  $property
     * @return mixed
     */
    public static function getValueIfSet(string $property_name, array $property): array;
}