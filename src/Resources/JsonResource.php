<?php

namespace PandaMan\JsonApiWrapper\Resources;

use PandaMan\JsonApiWrapper\Properties\BasicArrayPropertyUtility;
use PandaMan\JsonApiWrapper\Properties\BasicPropertyUtility;
use PandaMan\JsonApiWrapper\Properties\ResourceArrayPropertyUtility;
use PandaMan\JsonApiWrapper\Properties\ResourcePropertyUtility;

/**
 * Class JsonResource
 *
 * @package PandaMan\JsonApiWrapper\Resources
 */
abstract class JsonResource
{
    private array $internal_properties;
    private bool $is_in_override_mode;

    abstract protected function getPropertyDefinitions(): array;

    private array $property_utilities = [
        'basic'          => BasicPropertyUtility::class,
        'basic-array'    => BasicArrayPropertyUtility::class,
        'resource'       => ResourcePropertyUtility::class,
        'resource-array' => ResourceArrayPropertyUtility::class,
    ];

    public function __construct(array $data = [], bool $override = false, array $extended_property_utilities = [])
    {
        // update usable properties
        $this->property_utilities = array_merge($this->property_utilities, $extended_property_utilities);

        // set override mode
        $this->is_in_override_mode = $override;

        // build property definitions
        $this->unpackProperties();

        // load data if provided
        $this->_bulkLoadData($data);

        // reset data to unchanged when in override mode
        if ($this->is_in_override_mode) {
            $this->_reset();
        }

        // no override after construction
        $this->is_in_override_mode = false;
    }

    private function unpackProperties(): void
    {
        $basename = basename(get_class($this));

        foreach ($this->getPropertyDefinitions() as $property => $definition) {
            $definition    = explode('|', $definition);
            $type          = $definition[0];
            $has_default   = $this->_hasDefaultValue($type);
            $default_value = $this->_getDefaultValue($type);
            $type          = $this->_removeDefaultValueFromProperty($type);

            $this->internal_properties[$property] = [
                'id'          => "$basename:$property",
                'type'        => $type,
                'accepts'     => explode(',', $definition[1]),
                'rules'       => $definition[2] ?? false ? explode(',', $definition[2]) : null,
                'value'       => $default_value,
                'is_set'      => $has_default,
                'has_changed' => false,
            ];
        }
    }

    private function _bulkLoadData(array $data): void
    {
        foreach ($data as $property => $value) {
            $this->$property = $value;
        }
    }

    private function _reset(): void
    {
        foreach ($this->internal_properties as $property => $data) {
            $this->internal_properties[$property]['has_changed'] = false;
        }
    }

    private function _getDefaultValue(string $property): ?string
    {
        preg_match_all("/\[([^]]*)]/", $property, $matches);

        $value = $matches[1][0] ?? null;

        /**
         * TODO: note
         *
         * @example
         * basic[]
         *
         * Above returns a value of '' (empty string)
         * Let's set that to just be null for now. Not sure if a dev would want a default value of '' ever
         */
        if ($value === '') {
            $value = null;
        }

        return $value;
    }

    private function _hasDefaultValue(string $property): bool
    {
        preg_match_all("/\[([^]]*)]/", $property, $matches);

        return count($matches[1]) > 0;
    }

    private function _removeDefaultValueFromProperty(string $property): string
    {
        return preg_replace('/\[(.*?)]/', '', $property);
    }

    /**
     * @deprecated this method will not exist after beta
     */
    public function refresh()
    {
        $this->_reset();
    }

    public function __set($property, $value)
    {
        // skip property set attempt if in override mode and property doesn't exist
        if ($this->is_in_override_mode and !isset($this->internal_properties[$property])) {
            return;
        }

        $utility = $this->property_utilities[$this->internal_properties[$property]['type']];

        $this->internal_properties[$property]                = $utility::setValue($this->internal_properties[$property], $value, $this->is_in_override_mode);
        $this->internal_properties[$property]['has_changed'] = true;
    }

    public function __get($property)
    {
        $utility = $this->property_utilities[$this->internal_properties[$property]['type']];

        return $utility::getValue($this->internal_properties[$property]);
    }

    public function changedProperties(): array
    {
        foreach ($this->internal_properties ?? [] as $property => $data) {
            $utility = $this->property_utilities[$this->internal_properties[$property]['type']];
            $changes = array_merge($changes ?? [], $utility::getValueIfChanged($property, $data));
        }

        return $changes ?? [];
    }

    public function properties(): array
    {
        foreach ($this->internal_properties as $property => $data) {
            $utility = $this->property_utilities[$this->internal_properties[$property]['type']];
            $changes = array_merge($changes ?? [], $utility::getValueIfSet($property, $data));
        }

        return $changes ?? [];
    }
}