<?php

namespace PandaMan\JsonApiWrapper\Traits;

use Psr\Http\Message\ResponseInterface;

trait ResponseDataTrait
{
    protected int $status_code;

    protected string $message;

    protected string $body;

    protected array $headers;

    protected function setUpData(ResponseInterface $response): void
    {
        $this->status_code = $response->getStatusCode();
        $this->message     = $response->getReasonPhrase();
        $this->body        = $response->getBody()->getContents();
        $this->headers     = $response->getHeaders();
    }

    public function headers(): array
    {
        return $this->headers;
    }

    public function header($key): ?array
    {
        return $this->headers[ $key ] ?? null;
    }

    public function message(): string
    {
        return $this->message;
    }

    public function statusCode(): int
    {
        return $this->status_code;
    }

    public function wasSuccessful(): bool
    {
        return $this->status_code >= 200 and $this->status_code <= 299;
    }

    protected function body(bool $encoded = true)
    {
        return $encoded ? json_decode($this->body) : $this->body;
    }

    public function toString(): string
    {
        return $this->body;
    }

    public function toJson()
    {
        return json_decode($this->body);
    }

    public function toArray(): array
    {
        return json_decode($this->body, true);
    }

    /**
     * @deprecated going away in a future version, use toJson()
     */
    public function decodeBody()
    {
        return $this->body(true);
    }

    /**
     * @deprecated going away in a future version, use toString()
     */
    public function rawBody()
    {
        return $this->body(false);
    }
}