<?php


namespace PandaMan\JsonApiWrapper\Properties;

use PandaMan\JsonApiWrapper\Contracts\PropertyUtilityInterface;
use PandaMan\JsonApiWrapper\Resources\JsonResource;

class ResourcePropertyUtility implements PropertyUtilityInterface
{
    public static function getValue(array $property)
    {
        if (!$property['is_set']) {
            throw new \Exception("Attempting to access the value of a property that has not been set from {$property['id']}");
        }

        return $property['value'];
    }

    public static function setValue(array $property, $value, $override = false): array
    {
        if ($value instanceof $property['accepts'][0]) {
            // may want to rewrite
        } elseif ($override and is_array($value)) {
            $value = new $property['accepts'][0]($value, $override);
        } elseif (is_null($value)) {
            // may want to rewrite
        } else {
            throw new \Exception("Invalid type provided for {$property['id']}. Accepts: {$property['accepts'][0]}");
        }

        $property['value']  = $value;
        $property['is_set'] = true;

        return $property;
    }

    public static function getValueIfChanged(string $property_name, array $property): array
    {
        /** @var JsonResource $value */
        $value = $property['value'];

        return ($value and sizeof($value->changedProperties())) ? [$property_name => $value->changedProperties()] : [];
    }

    public static function getValueIfSet(string $property_name, array $property): array
    {
        /** @var JsonResource $value */
        $value = $property['value'];

        return ($value and sizeof($properties = $value->properties())) ? [$property_name => $properties] : [];
    }
}