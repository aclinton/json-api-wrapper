<?php


namespace PandaMan\JsonApiWrapper\Properties;


use PandaMan\JsonApiWrapper\Contracts\PropertyUtilityInterface;
use PandaMan\JsonApiWrapper\Resources\JsonResource;

/**
 * Class ResourceArrayPropertyUtility
 *
 * @package PandaMan\JsonApiWrapper\Properties
 */
class ResourceArrayPropertyUtility implements PropertyUtilityInterface
{
    public static function getValueIfChanged(string $property_name, array $property): array
    {
        foreach ($property['value'] ?? [] as $value) {
            /** @var JsonResource $value */

            if (sizeof($changed_properties = $value->changedProperties())) {
                $changes[] = $changed_properties;
            }
        }

        return sizeof($changes ?? []) ? [$property_name => $changes ?? []] : [];
    }

    public static function getValue(array $property)
    {
        if (!$property['is_set']) {
            throw new \Exception("Attempting to access the value of a property that has not been set from {$property['id']}");
        }

        return $property['value'];
    }

    public static function setValue(array $property, $value, $override = false): array
    {
        foreach ($value ?? [] as $key => $val) {
            if ($val instanceof $property['accepts'][0]) {
                // may want to rewrite
            } elseif ($override and is_array($val)) {
                $value[ $key ] = new $property['accepts'][0]($val, $override);
            } elseif (is_null($val)) {
                unset($value[ $key ]);
            } else {
                throw new \Exception("Invalid type provided for {$property['id']}. Accepts an array of {$property['accepts'][0]}");
            }
        }


        $property['value']  = $value;
        $property['is_set'] = true;

        return $property;
    }

    public static function getValueIfSet(string $property_name, array $property): array
    {
        foreach ($property['value'] ?? [] as $value) {
            /** @var JsonResource $value */

            if (sizeof($properties = $value->properties())) {
                $changes[] = $properties;
            }
        }

        return sizeof($changes ?? []) ? [$property_name => $changes ?? []] : [];
    }
}