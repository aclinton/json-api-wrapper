<?php


namespace PandaMan\JsonApiWrapper\Properties;


use PandaMan\JsonApiWrapper\Contracts\PropertyUtilityInterface;

class BasicPropertyUtility implements PropertyUtilityInterface
{
    public static function getValue(array $property)
    {
        if (!$property['is_set']) {
            throw new \Exception("Attempting to access the value of a property that has not been set from {$property['id']}");
        }

        return $property['value'];
    }

    public static function setValue(array $property, $value, $override = false): array
    {
        if (!(in_array(gettype($value), $property['accepts']) or $override or $value === null)) {
            $display_value = in_array($type = gettype($value), ['object', 'array', 'boolean']) ? $type : $value;
            throw  new \Exception("Invalid value type provided ($display_value) for {$property['id']}. Accepts: " . implode(',', $property['accepts']));
        }

        $property['value']  = $value;
        $property['is_set'] = true;

        return $property;
    }

    public static function getValueIfChanged(string $property_name, array $property): array
    {
        return $property['has_changed'] ? [$property_name => $property['value']] : [];
    }

    public static function getValueIfSet(string $property_name, array $property): array
    {
        return $property['is_set'] ? [$property_name => $property['value']] : [];
    }
}