<?php


namespace PandaMan\JsonApiWrapper\Properties;


use PandaMan\JsonApiWrapper\Contracts\PropertyUtilityInterface;

class BasicArrayPropertyUtility implements PropertyUtilityInterface
{
    public static function getValue(array $property)
    {
        if (!$property['is_set']) {
            throw new \Exception("Attempting to access the value of a property that has not been set from {$property['id']}");
        }

        return $property['value'];
    }

    public static function setValue(array $property, $value, $override = false): array
    {
        if (!is_array($value)) {
            throw new \Exception("Invalid value provided for {$property['id']}. Must provide an array");
        }

        foreach ($value as $val) {
            if (!(in_array(gettype($val), $property['accepts']) or $override)) {
                $display_value = in_array($type = gettype($val), ['object', 'array', 'boolean']) ? $type : $val;
                throw  new \Exception("Invalid value type provided ($display_value) for {$property['id']}. Accepts: " . implode(',', $property['accepts']));
            }
        }

        $property['value']  = $value;
        $property['is_set'] = true;

        return $property;
    }

    public static function getValueIfChanged(string $property_name, array $property): array
    {
        return $property['has_changed'] ? [$property_name => $property['value']] : [];
    }

    public static function getValueIfSet(string $property_name, array $property): array
    {
        return $property['is_set'] ? [$property_name => $property['value']] : [];
    }
}