<?php


namespace Tests\Dummies;

use PandaMan\JsonApiWrapper\Responses\BaseCountResponse;

class DummyCountResponse extends BaseCountResponse
{
    /**
     * Count
     *
     * @return int|null
     */
    public function count(): ?int
    {
        return json_decode($this->rawBody(), true)['dummies'] ?? null;
    }
}