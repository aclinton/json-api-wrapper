<?php


namespace Tests\Dummies;


use PandaMan\JsonApiWrapper\Responses\SingleResourceResponse;
use Tests\Dummies\DummyResource;

class DummySingleResourceResponse extends SingleResourceResponse
{

    /**
     * Get Data
     * d
     * Define what the root data point is. This will be passed into the base resource constructor
     *
     * @param array $base_data
     * @return array
     */
    protected function getData(array $base_data): array
    {
        return $base_data;
    }

    /**
     * Get Base Resource
     *
     * Define what the base resource object is.
     *
     * @return string
     */
    protected function getBaseResource(): string
    {
        return DummyResource::class;
    }
}