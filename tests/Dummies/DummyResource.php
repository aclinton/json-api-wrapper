<?php


namespace Tests\Dummies;


use PandaMan\JsonApiWrapper\Resources\JsonResource;

/**
 * Class DummyResource
 *
 * This class should not be used outside of testing
 *
 * @package Tests
 * @property string $basic_string
 * @property int $basic_int
 * @property bool $basic_bool
 * @property double $basic_double
 * @property string|int|double|bool $basic_mix
 * @property string[] $basic_string_array
 * @property int[] $basic_int_array
 * @property DummyResource $resource
 * @property DummyResource[] $resource_array
 */
class DummyResource extends JsonResource
{
    protected function getPropertyDefinitions(): array
    {
        return [
            'basic_string'       => 'basic|string',
            'basic_int'          => 'basic|integer',
            'basic_bool'         => 'basic|boolean',
            'basic_double'       => 'basic|double',
            'basic_mix'          => 'basic|double,string,integer,boolean',
            'basic_string_array' => 'basic-array|string',
            'basic_int_array'    => 'basic-array|integer',
            'resource'           => 'resource|' . DummyResource::class,
            'resource_array'     => 'resource-array|' . DummyResource::class,
        ];
    }
}