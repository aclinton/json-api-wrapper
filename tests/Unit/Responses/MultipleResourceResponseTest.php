<?php

namespace Tests\Unit\Responses;

use PandaMan\JsonApiWrapper\Resources\JsonResource;
use GuzzleHttp\Psr7\Response;
use Tests\Dummies\DummyMultipleResourceResponse;
use Tests\Dummies\DummyResource;
use Tests\Dummies\DummySingleResourceResponse;
use Tests\TestCase;

class MultipleResourceResponseTest extends TestCase
{
    /** @test */
    public function can_get_successful_response()
    {
        $payload = json_encode([
            [
                'basic_string' => 'test 1',
            ],
            [
                'basic_string' => 'test 2',
            ],
            [
                'basic_string' => 'test 3',
            ],
        ]);

        // create response
        $response = new DummyMultipleResourceResponse(new Response(200, [], $payload, null, "Test response message."));

        /** @var DummyResource[] $data */
        $data = $response->data();

        // make assertions
        $this->assertEquals(200, $response->statusCode());
        $this->assertTrue($response->wasSuccessful());
        $this->assertEquals("Test response message.", $response->message());
        $this->assertTrue(is_array($data));
        $this->assertEquals('test 1', $data[0]->basic_string);
        $this->assertEquals('test 2', $data[1]->basic_string);
        $this->assertEquals('test 3', $data[2]->basic_string);
        $this->assertInstanceOf(JsonResource::class, $data[0]);
        $this->assertInstanceOf(JsonResource::class, $data[1]);
        $this->assertInstanceOf(JsonResource::class, $data[2]);
    }

    /** @test */
    public function can_get_404_response()
    {
        // create response
        $response = new DummyMultipleResourceResponse(new Response(404, [], "{}", null, "Not found"));

        // make assertions
        $this->assertEquals(404, $response->statusCode());
        $this->assertFalse($response->wasSuccessful());
        $this->assertEquals("Not found", $response->message());
        $this->assertEquals(0, sizeof($response->data()));
    }
}