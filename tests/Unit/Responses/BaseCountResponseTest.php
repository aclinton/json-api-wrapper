<?php

namespace Tests\Unit\Responses;

use GuzzleHttp\Psr7\Response;
use Tests\Dummies\DummyCountResponse;
use Tests\TestCase;

class BaseCountResponseTest extends TestCase
{
    /** @test */
    public function can_get_successful_response()
    {
        // create response
        $response = new DummyCountResponse(new Response(200, [], '{"dummies":5}', null, "Test response message."));

        // make assertions
        $this->assertEquals(200, $response->statusCode());
        $this->assertEquals(5, $response->count());
        $this->assertTrue($response->wasSuccessful());
        $this->assertEquals("Test response message.", $response->message());
    }

    /** @test */
    public function can_get_404_response()
    {
        // create response
        $response = new DummyCountResponse(new Response(404, [], "{}", null, "Not found"));

        // make assertions
        $this->assertEquals(404, $response->statusCode());
        $this->assertFalse($response->wasSuccessful());
        $this->assertEquals("Not found", $response->message());
    }
}