<?php

namespace Tests\Unit\Responses;

use PandaMan\JsonApiWrapper\Resources\JsonResource;
use GuzzleHttp\Psr7\Response;
use Tests\Dummies\DummyResource;
use Tests\Dummies\DummySingleResourceResponse;
use Tests\TestCase;

class SingleResourceResponseTest extends TestCase
{
    /** @test */
    public function can_get_successful_response()
    {
        $payload = json_encode([
            'basic_string' => 'test',
        ]);

        // create response
        $response = new DummySingleResourceResponse(
            new Response(
                200,
                [],
                $payload,
                null,
                "Test response message.")
        );

        /** @var DummyResource $data */
        $data = $response->data();

        // make assertions
        $this->assertEquals(200, $response->statusCode());
        $this->assertTrue($response->wasSuccessful());
        $this->assertEquals("Test response message.", $response->message());
        $this->assertEquals('test', $data->basic_string);
        $this->assertInstanceOf(JsonResource::class, $response->data());
    }

    /** @test */
    public function can_get_404_response()
    {
        // create response
        $response = new DummySingleResourceResponse(new Response(404, [], "{}", null, "Not found"));

        // make assertions
        $this->assertEquals(404, $response->statusCode());
        $this->assertFalse($response->wasSuccessful());
        $this->assertEquals("Not found", $response->message());
        $this->assertNull($response->data());
    }
}