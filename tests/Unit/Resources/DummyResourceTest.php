<?php

namespace Tests\Unit\Resources;

use Tests\Dummies\DummyResource;

class DummyResourceTest extends \Tests\TestCase
{
    /** @test */
    public function can_set_properties()
    {
        $dummy_resource                     = new DummyResource;
        $dummy_resource->basic_bool         = true;
        $dummy_resource->basic_double       = 2.05;
        $dummy_resource->basic_int          = 7;
        $dummy_resource->basic_string       = "test string";
        $dummy_resource->basic_int_array    = [1, 55, 6];
        $dummy_resource->basic_string_array = ['one', 'two'];
        $dummy_resource->resource           = new DummyResource;
        $dummy_resource->resource_array     = [new DummyResource(['basic_bool' => false]), new DummyResource];

        $this->assertEquals(true, $dummy_resource->basic_bool);
        $this->assertEquals(2.05, $dummy_resource->basic_double);
        $this->assertEquals(7, $dummy_resource->basic_int);
        $this->assertEquals("test string", $dummy_resource->basic_string);
        $this->assertTrue(is_array($dummy_resource->basic_int_array));
        $this->assertTrue(is_array($dummy_resource->basic_string_array));
        $this->assertInstanceOf(DummyResource::class, $dummy_resource->resource);
        $this->assertTrue(is_array($dummy_resource->resource_array));
        $this->assertInstanceOf(DummyResource::class, $dummy_resource->resource_array[0]);
        $this->assertInstanceOf(DummyResource::class, $dummy_resource->resource_array[1]);
        $this->assertEquals(false, $dummy_resource->resource_array[0]->basic_bool);
    }

    /** @test */
    public function cannot_set_invalid_basic_properties()
    {
        $dummy_resource = new DummyResource;

        try {
            $dummy_resource->basic_string = 7;
        } catch (\Exception $exception) {
            $e = $exception;
        }

        $this->assertInstanceOf(\Exception::class, $e ?? null);
    }

    /** @test */
    public function cannot_set_invalid_basic_array_properties()
    {
        $dummy_resource = new DummyResource;

        try {
            $dummy_resource->basic_string_array = 7;
        } catch (\Exception $exception) {
            $e = $exception;
        }

        $this->assertInstanceOf(\Exception::class, $e ?? null);
    }

    /** @test */
    public function cannot_set_invalid_basic_array_properties_of_incorrect_type()
    {
        $dummy_resource = new DummyResource;

        try {
            $dummy_resource->basic_string_array = [1, 2, 3];
        } catch (\Exception $exception) {
            $e = $exception;
        }

        $this->assertInstanceOf(\Exception::class, $e ?? null);
    }

    /** @test */
    public function cannot_set_invalid_resource_properties()
    {
        $dummy_resource = new DummyResource;

        try {
            $dummy_resource->resource = 'not a resource';
        } catch (\Exception $exception) {
            $e = $exception;
        }

        $this->assertInstanceOf(\Exception::class, $e ?? null);
    }

    /** @test */
    public function cannot_set_invalid_resource_array_properties()
    {
        $dummy_resource = new DummyResource;

        try {
            $dummy_resource->resource_array = ['not a resource'];
        } catch (\Exception $exception) {
            $e = $exception;
        }

        $this->assertInstanceOf(\Exception::class, $e ?? null);
    }

    /** @test */
    public function can_set_basic_mixed_value()
    {
        $dummy_resource = new DummyResource;

        try {
            $dummy_resource->basic_mix = 1;
            $dummy_resource->basic_mix = "one";
            $dummy_resource->basic_mix = false;
            $dummy_resource->basic_mix = true;
            $dummy_resource->basic_mix = 1.33;
        } catch (\Exception $exception) {
            $e = $exception;
        }

        $this->assertNull($e ?? null);
    }

    /** @test */
    public function can_set_data_with_array()
    {
        $dummy_resource = new DummyResource([
            'basic_string'       => 'test string',
            'basic_int'          => 22,
            'basic_bool'         => true,
            'basic_double'       => 3.3375,
            'basic_mix'          => 'test mix',
            'basic_string_array' => ['one', 'fish', 'two', 'fish'],
            'basic_int_array'    => [8, 6, 7, 5, 3, 0, 9],
            'resource'           => new DummyResource,
            'resource_array'     => [new DummyResource(['basic_int' => 54321]), new DummyResource()],
        ]);

        $this->assertEquals("test string", $dummy_resource->basic_string);
        $this->assertEquals(22, $dummy_resource->basic_int);
        $this->assertEquals(true, $dummy_resource->basic_bool);
        $this->assertEquals(3.3375, $dummy_resource->basic_double);
        $this->assertEquals('test mix', $dummy_resource->basic_mix);
        $this->assertTrue(is_array($dummy_resource->basic_int_array));
        $this->assertTrue(is_array($dummy_resource->basic_string_array));
        $this->assertInstanceOf(DummyResource::class, $dummy_resource->resource);
        $this->assertTrue(is_array($dummy_resource->resource_array));
        $this->assertInstanceOf(DummyResource::class, $dummy_resource->resource_array[0]);
        $this->assertInstanceOf(DummyResource::class, $dummy_resource->resource_array[1]);
        $this->assertEquals(54321, $dummy_resource->resource_array[0]->basic_int);
    }

    /** @test */
    public function can_set_nullable_properties_to_null()
    {
        $dummy_resource                 = new DummyResource;
        $dummy_resource->basic_int      = null;
        $dummy_resource->basic_string   = null;
        $dummy_resource->basic_bool     = null;
        $dummy_resource->basic_double   = null;
        $dummy_resource->basic_mix      = null;
        $dummy_resource->resource       = null;
        $dummy_resource->resource_array = null;

        $this->assertNull($dummy_resource->basic_int);
        $this->assertNull($dummy_resource->basic_string);
        $this->assertNull($dummy_resource->basic_bool);
        $this->assertNull($dummy_resource->basic_double);
        $this->assertNull($dummy_resource->basic_mix);
        $this->assertNull($dummy_resource->resource);
        $this->assertNull($dummy_resource->resource_array);
    }

    /** @test */
    public function can_set_resource_values_with_array_on_construction_in_override_mode()
    {
        $dummy_resource = new DummyResource([
            'resource'       => [
                'basic_string' => 'test nested resource',
            ],
            'resource_array' => [
                ['basic_int' => 54321],
                [],
            ],
        ], true);

        $this->assertInstanceOf(DummyResource::class, $dummy_resource->resource);
        $this->assertTrue(is_array($dummy_resource->resource_array));
        $this->assertInstanceOf(DummyResource::class, $dummy_resource->resource_array[0]);
        $this->assertInstanceOf(DummyResource::class, $dummy_resource->resource_array[1]);
        $this->assertEquals(54321, $dummy_resource->resource_array[0]->basic_int);
    }

    /** @test */
    public function can_set_properties_as_unchanged_but_set_in_override_mode()
    {
        $dummy_resource = new DummyResource([
            'basic_string'       => 'test string',
            'basic_int'          => 22,
            'basic_bool'         => true,
            'basic_double'       => 3.3375,
            'basic_mix'          => 'test mix',
            'basic_string_array' => ['one', 'fish', 'two', 'fish'],
            'basic_int_array'    => [8, 6, 7, 5, 3, 0, 9],
            'resource'           => new DummyResource,
            'resource_array'     => [new DummyResource(['basic_int' => 54321], true), new DummyResource([], true)],
        ], true);

        $this->assertEquals(0, sizeof($dummy_resource->changedProperties()));
        $this->assertGreaterThan(0, sizeof($dummy_resource->properties()));
    }

    /** @test */
    public function can_set_undefined_property_in_override_mode_without_exception_being_thrown()
    {
        try {
            new DummyResource([
                'undefined_property' => 'any value',
            ], true);
        } catch (\Exception $exception) {
            $e = $exception;
        }

        $this->assertNull($e ?? null);
    }

    /** @test */
    public function cannot_set_undefined_property_when_not_in_override_mode_through_constructor()
    {
        try {
            new DummyResource([
                'undefined_property' => 'any value',
            ]);
        } catch (\Exception $exception) {
            $e = $exception;
        }

        $this->assertInstanceOf(\Exception::class, $e ?? null);
    }

    /** @test */
    public function cannot_set_undefined_property_outside_of_constructor()
    {
        try {
            $dummy_resource = new DummyResource();
            $dummy_resource->undefined_property = 'any value';
        } catch (\Exception $exception) {
            $e = $exception;
        }

        $this->assertInstanceOf(\Exception::class, $e ?? null);
    }
}