# JSON API Wrapper
### Purpose
This PHP package can be used as a base for creating an API wrapper for a JSON API (especially REST based), which can provide basic patterns and auto completion in an IDE.

Examples:

- [ReCharge API Wrapper](https://gitlab.bvaccelapps.com/fullstack/php-packages/recharge-api-wrapper)
- [Laravel Forge API Wrapper](https://gitlab.bvaccelapps.com/fullstack/php-packages/forge-api-wrapper)

## Installation
The JSON API Wrapper is available via the [BVA private Satis repo](https://packages.bvaccelapps.com)

To install add the following to the `composer.json` file:



```json
{
    ...
    "repositories": [
        {
            "type": "composer",
            "url": "https://packages.bvaccelapps.com"
        }
    ],
}
```

The run the command `composer require bvaccel/json-api-wrapper`

## Use

#### Clients

#### Requests

#### Resources

Resources represent the JSON objects provided to the API in requests and return by the API in responses. Before creating an object it's a good idea to create a base object to encapsulate the config features of the Resource object.

```php
<?php

namespace  BVAccel\NewApiWrapper\Resources;

use PandaMan\JsonApiWrapper\Resources\JsonResource;

abstract class NewApiResource extends JsonResource
{
    /**
     * NewApiReseource constructor.
     *
     * @param array $data
     * @param bool  $override
     */
    public function __construct(array  $data = [], $override = false) {

        $optional_properties = [
            'special-string' => SpecialStringProperty::class
        ];

        parent::__construct($data, $override);
    }
}
```

#### Notes on Using with Laravel